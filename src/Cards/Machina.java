package Cards;

import Player.Player;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 9:10 PM
 */
public class Machina extends AbstractCard {

    private DiceDiscs discs;

    public Machina(DiceDiscs discs) {
        super(CardID.MACHINA);
        this.discs = discs;
    }

    @Override
    public void activate(Player owner) {
        List<DiceDisc> discs = this.discs.getDiscs();
        List<Card> cards = new ArrayList<Card>();

        //pick up all the cards
        for (DiceDisc d : discs) {
            Card c = d.getLaidCard(owner);
            if (c != null && c.getCardType() == CardType.BUILDING) {
                assert(d.removeCardFromDiceDisc(owner) == c);
                cards.add(c);
            }
        }

        //and lay them down
        Iterator<Card> c = cards.iterator();
        if (c.hasNext()) {
            Card card = c.next(); //get the first one
            while (c.hasNext()) {
                int i = owner.pickDiceDisc();
                DiceDisc d = this.discs.getDisc(i);
                if (d.layCard(owner, card)) {
                    card = c.next();
                }
            }
        }
    }
}
