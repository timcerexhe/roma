package Cards;

import Player.Player;
import RomaEngine.AddressOfArena;
import RomaEngine.Arena;
import RomaEngine.HandicapArena;
import RomaEngine.OpponentMap;

import java.util.HashMap;
import java.util.Map;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 2:11 AM
 */
public class Essedum extends AbstractCard {

    public static final int OPPONENT_MODIFIER = -2;

    private AddressOfArena arena;
    private Arena wrapper;
    private OpponentMap opponents;

    public Essedum(AddressOfArena arena, OpponentMap opponents) {
        super(CardID.ESSEDUM);
        this.arena = arena;
        this.wrapper = null;
        this.opponents = opponents;
    }

    @Override
    public void activate(Player owner) {
        Arena a = this.arena.getArena();
        Map<Player, Integer> handicap = new HashMap<Player, Integer>();
        Player opponent = this.opponents.getOpponentOf(owner);
        handicap.put(opponent, OPPONENT_MODIFIER);
        this.wrapper = new HandicapArena(a, handicap);
        this.arena.setArena(this.wrapper);
    }

    @Override
    public void endOfTurn() {
        assert(this.wrapper != null);
        Arena a = this.arena.getArena();
        a = a.unwrap(this.wrapper);
        this.arena.setArena(a);

        this.wrapper = null;
    }

}
