package Cards;

import Cards.CardType;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 6:19 PM
 */
public enum CardID {
    //character cards
    SICARIUS        (CardType.CHARACTER, "Sicarius",        1, 1),
    ARCHITECTUS     (CardType.CHARACTER, "Architectus",     1, 1),
    CONSILLIARIUS   (CardType.CHARACTER, "Consilliarius",   1, 1),
    LEGAT           (CardType.CHARACTER, "Legat",           1, 1),
    GLADIATOR       (CardType.CHARACTER, "Gladiator",       1, 1),
    MERCATOR        (CardType.CHARACTER, "Mercator",        1, 1),
    CONSUL          (CardType.CHARACTER, "Consul",          1, 1),
    LEGIONARIUS     (CardType.CHARACTER, "Legionarius",     1, 1),
    NERO            (CardType.CHARACTER, "Nero",            1, 1),
    PRAETORIANUS    (CardType.CHARACTER, "Praetorianus",    1, 1),
    SCAENICUS       (CardType.CHARACTER, "Scaenicus",       1, 1),
    HARUSPEX        (CardType.CHARACTER, "Haruspex",        1, 1),
    SENATOR         (CardType.CHARACTER, "Senator",         1, 1),
    VELITES         (CardType.CHARACTER, "Velites",         1, 1),
    ESSEDUM         (CardType.CHARACTER, "Essedum",         1, 1),
    TRIBUNUS_PLEBIS (CardType.CHARACTER, "Tribunus Plebis", 1, 1),
    CENTURIO        (CardType.CHARACTER, "Centurio",        1, 1),

    //building cards
    AESCULAPINUM    (CardType.BUILDING,  "Aesculapinum",    1, 1),
    BASILICA        (CardType.BUILDING,  "Basilica",        1, 1),
    MACHINA         (CardType.BUILDING,  "Machina",         1, 1),
    FORUM           (CardType.BUILDING,  "Forum",           1, 1),
    MERCATUS        (CardType.BUILDING,  "Mercatus",        1, 1),
    ONAGER          (CardType.BUILDING,  "Onager",          1, 1),
    TEMPLUM         (CardType.BUILDING,  "Templum",         1, 1),
    TURRIS          (CardType.BUILDING,  "Turris",          1, 1);


    private CardType cardType;
    private int price;
    private int defenceLevel;
    private String name;

    private CardID(CardType type, String name, int price, int defenceLevel) {
        this.cardType = type;
        this.price = price;
        this.defenceLevel = defenceLevel;
        this.name = name;
    }

    public CardType getCardType() {
        return this.cardType;
    }

    public int getPrice() {
        return this.price;
    }

    public int getDefenceLevel() {
        return this.defenceLevel;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
