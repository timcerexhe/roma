package Cards;

import Player.Player;

/**
 * User: timothyc
 * Date: 5/18/12
 * Time: 1:47 PM
 */
public abstract class AbstractCard implements Card {

    private CardID cardID;

    public AbstractCard(CardID cardID) {
        this.cardID = cardID;
    }

    @Override
    public CardID getCardID() {
        return this.cardID;
    }

    @Override
    public CardType getCardType() {
        return this.cardID.getCardType();
    }

    @Override
    public int getPrice() {
        return this.cardID.getPrice();
    }

    @Override
    public int getDefenceLevel() {
        return this.cardID.getDefenceLevel();
    }

    @Override
    public void endOfTurn() {
        //most cards are instantaneous --> do nothing at end of turn
        //we do the nothing here to save time later on ... ;)
    }

    @Override
    public void onPlacement(Player owner, int discNumber) {
    }

    @Override
    public void onRemove() {
    }

}
