package Cards;

import Player.Player;
import RomaEngine.Bank;
import RomaEngine.OpponentMap;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 1:17 AM
 */
public class Mercator extends AbstractCard {

    //2 sestertii : 1 victory point
    public static final int CONVERSION_RATE = 2;

    private Bank bank;
    private OpponentMap opponents;

    public Mercator(Bank bank, OpponentMap opponents) {
        super(CardID.MERCATOR);
        this.bank = bank;
        this.opponents = opponents;
    }

    @Override
    public void activate(Player owner) {
        Player opponent = this.opponents.getOpponentOf(owner);
        int available = this.bank.getPlayerVictoryPoints(opponent);
        int funds = this.bank.getPlayerSestertii(owner);
        int maxVictoryPoints = Math.max(funds / CONVERSION_RATE, available);

        int amountVictoryPoints = owner.pickInRange(0, maxVictoryPoints);
        int amountSestertii = amountVictoryPoints * CONVERSION_RATE;

        this.bank.transferSestertii(owner, opponent, amountSestertii);
        this.bank.transferVictoryPoints(opponent, owner, amountVictoryPoints);
    }
}
