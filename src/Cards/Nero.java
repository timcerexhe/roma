package Cards;

import Player.Player;
import RomaEngine.AddressOfArena;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;
import RomaEngine.OpponentMap;

import java.util.List;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 6:50 PM
 */
public class Nero extends AbstractCard {

    private DiceDiscs discs;
    private AddressOfArena arena;
    private OpponentMap opponents;

    public Nero(DiceDiscs discs, AddressOfArena arena, OpponentMap opponents) {
        super(CardID.NERO);
        this.discs = discs;
        this.arena = arena;
        this.opponents = opponents;
    }

    @Override
    public void activate(Player owner) {
        Player opponent = this.opponents.getOpponentOf(owner);
        int numCandidates = getNumCandidates(opponent, this.discs.getDiscs());

        if (numCandidates == 0) {
            owner.receiveNotification("cannot activate Nero -- "+opponent.getName()+" has no buildings");

        } else {
            DiceDisc target = null;
            while (target == null) {
                int discNumber = owner.pickDiceDisc();
                DiceDisc selected = this.discs.getDisc(discNumber);
                Card attacked = selected.getLaidCard(opponent);
                if (attacked != null && attacked.getCardType() == CardType.BUILDING) {
                    target = selected;
                }
            }

            //TODO notify users (a la Velites)
            this.arena.getArena().discard(opponent, target);
            this.arena.getArena().discard(owner, this.discs.getDiscHolding(owner, this));
        }
    }

    private int getNumCandidates(Player opponent, List<DiceDisc> discs) {
        int count = 0;
        for (DiceDisc d : discs) {
            if (d.getLaidCard(opponent).getCardType() == CardType.BUILDING) {
                count++;
            }
        }
        return count;
    }
}
