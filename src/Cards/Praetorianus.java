package Cards;

import Player.Player;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;
import RomaEngine.OpponentMap;

import java.util.ArrayList;
import java.util.List;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 7:01 PM
 */
public class Praetorianus extends AbstractCard {

    private DiceDiscs discs;
    private OpponentMap opponents;

    private DiceDisc wrapped;
    private int discIndex;

    public Praetorianus(DiceDiscs discs, OpponentMap opponents) {
        super(CardID.PRAETORIANUS);
        this.discs = discs;
        this.opponents = opponents;
    }

    @Override
    public void activate(Player owner) {
        int discNumber = owner.pickDiceDisc();
        this.discIndex = discNumber - 1;
        List<DiceDisc> discs = this.discs.getDiscs();
        DiceDisc original = discs.remove(discIndex);
        this.wrapped = new BlockedDiceDisc(original, this.opponents.getOpponentOf(owner));

        discs.remove(this.discIndex);
        discs.add(this.discIndex, original);
    }

    @Override
    public void endOfTurn() {
        if (this.wrapped != null) {
            List<DiceDisc> discs = this.discs.getDiscs();
            DiceDisc original = discs.get(this.discIndex).unwrap(this.wrapped);
            discs.remove(this.discIndex);
            discs.add(this.discIndex, original);
        }
    }
}
