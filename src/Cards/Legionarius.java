package Cards;

import Player.Player;
import RomaEngine.AddressOfArena;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;
import RomaEngine.OpponentMap;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 5:53 PM
 */
public class Legionarius extends AbstractCard {

    private DiceDiscs discs;
    private AddressOfArena arena;
    private OpponentMap opponents;

    public Legionarius(DiceDiscs discs, AddressOfArena arena, OpponentMap opponents) {
        super(CardID.LEGIONARIUS);
        this.discs = discs;
        this.arena = arena;
        this.opponents = opponents;
    }

    @Override
    public void activate(Player owner) {
        DiceDisc disc = this.discs.getDiscHolding(owner, this);
        assert(disc != null);
        Player opponent = this.opponents.getOpponentOf(owner);
        Card defender = disc.getLaidCard(opponent);
        int defence = this.arena.getArena().getDefenceLevel(opponent, disc);
        int attack = this.arena.getArena().getAttackLevel(owner, disc);
        if (attack >= defence) {
            this.arena.getArena().discard(opponent, disc);
        }
    }
}
