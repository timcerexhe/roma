package Cards;

import Player.Player;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;
import RomaEngine.OpponentMap;

import java.util.List;
import java.util.Map;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 1:10 AM
 */
public class Gladiator extends AbstractCard {

    private DiceDiscs discs;
    private OpponentMap opponents;
    private Map<Player, List<Card>> hands;

    public Gladiator(DiceDiscs discs, OpponentMap opponents, Map<Player, List<Card>> hands) {
        super(CardID.GLADIATOR);
        this.discs = discs;
        this.opponents = opponents;
        this.hands = hands;
    }

    @Override
    public void activate(Player owner) {
        DiceDisc disc = null;
        Card target = null;
        Player opponent = this.opponents.getOpponentOf(owner);
        while (target == null) { //TODO what if the opponent doesn't have any face up cards!? infinite loop!
            int discNumber = owner.pickDiceDisc();
            disc = this.discs.getDisc(discNumber);
            target = disc.getLaidCard(opponent);
        }
        assert(disc != null);

        Card disposed = disc.removeCardFromDiceDisc(opponent);
        this.hands.get(opponent).add(disposed);
    }
}
