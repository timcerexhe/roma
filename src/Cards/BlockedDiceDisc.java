package Cards;

import Player.Player;
import RomaEngine.DiceDisc;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 7:11 PM
 */
public class BlockedDiceDisc implements DiceDisc {

    private DiceDisc base;
    private Player blocked;

    public BlockedDiceDisc(DiceDisc original, Player blocked) {
        this.base = original;
    }

    @Override
    public Card getLaidCard(Player p) {
        return this.base.getLaidCard(p);
    }

    @Override
    public Card removeCardFromDiceDisc(Player owner) {
        return this.base.removeCardFromDiceDisc(owner);
    }

    @Override
    public boolean layCard(Player p, Card c) {
        if (p == this.blocked) {
            p.receiveNotification("cannot lay "+c+" on this dice disc -- it has been blocked");
            return false;
        } else {
            return this.base.layCard(p, c);
        }
    }

    @Override
    public DiceDisc unwrap(DiceDisc wrapper) {
        if (wrapper == this) {
            return this.base;
        } else {
            this.base = this.base.unwrap(wrapper);
            return this;
        }
    }
}
