package Cards;

import Player.Player;
import RomaEngine.Bank;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;
import RomaEngine.OpponentMap;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 9:19 PM
 */
public class Mercatus extends AbstractCard {

    private Bank bank;
    private DiceDiscs discs;
    private OpponentMap opponents;

    public Mercatus(Bank bank, DiceDiscs diceDiscs, OpponentMap opponents) {
        super(CardID.MERCATUS);
        this.bank = bank;
        this.discs = diceDiscs;
        this.opponents = opponents;
    }

    @Override
    public void activate(Player owner) {
        int amount = 0;
        Player opponent = this.opponents.getOpponentOf(owner);
        for (DiceDisc d : this.discs.getDiscs()) {
            Card c = d.getLaidCard(opponent);
            if (c.getCardID() == CardID.FORUM) {
                amount++;
            }
        }
        this.bank.receiveStockpileVictoryPoints(owner, amount);
    }
}
