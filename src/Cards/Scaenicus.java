package Cards;

import Player.Player;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;
import RomaEngine.DiceManager;

import java.util.List;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 4:16 PM
 */
public class Scaenicus extends AbstractCard {

    private DiceDiscs discs;
    private Card base;

    public Scaenicus(DiceDiscs discs) {
        super(CardID.SCAENICUS);
        this.discs = discs;
        this.base = null;
    }

    @Override
    public void activate(Player owner) {
        int occupied = this.discs.getOccupiedDiscs(owner);
        this.base = null;

        if (occupied <= 1) { //make sure we aren't the ONLY card to activate!
            owner.receiveNotification("There are no other face-up cards available for Scaenicus to act upon");

        } else {
            DiceDisc selected = null;
            while (selected == null) {
                int discNumber = owner.pickDiceDisc();
                selected = this.discs.getDisc(discNumber);
                if (selected.getLaidCard(owner) == null) { //no good!
                    selected = null;
                }
            }

            this.base = selected.getLaidCard(owner);
            this.base.activate(owner);
        }
    }

    @Override
    public void endOfTurn() {
        if (this.base != null) {
            this.base.endOfTurn();
            this.base = null;
        }
    }
}
