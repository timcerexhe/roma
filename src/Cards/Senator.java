package Cards;

import Player.Player;
import RomaEngine.AddressOfEstateAgent;
import RomaEngine.EstateAgent;
import RomaEngine.FreeEstateAgent;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 8:03 PM
 */
public class Senator extends AbstractCard {

    private AddressOfEstateAgent agent;
    private FreeEstateAgent wrapped;

    public Senator(AddressOfEstateAgent agent) {
        super(CardID.SENATOR);
        this.agent = agent;
        this.wrapped = null;
    }

    @Override
    public void activate(Player owner) {
        this.wrapped = new FreeEstateAgent(agent.getAgent(), owner, true, false);
        this.agent.setAgent(this.wrapped);
    }

    @Override
    public void endOfTurn() {
        if (this.wrapped != null) {
            EstateAgent unwrapped = this.agent.getAgent().unwrap(this.wrapped);
            this.agent.setAgent(unwrapped);
        }
    }
}
