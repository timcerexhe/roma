package Cards;

import Player.Player;
import RomaEngine.DiceManager;
import RomaEngine.Die;
import RomaEngine.DieModifier;

import java.util.List;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 1:38 AM
 */
public class Consul extends AbstractCard {

    private DiceManager dice;
    private Die modified;

    public Consul(DiceManager dice) {
        super(CardID.CONSUL);
        this.dice = dice;
    }

    @Override
    public void activate(Player owner) {
        List<Integer> values = this.dice.getRemainingDiceValues(owner);
        assert(values.size() > 0);
        int target = owner.pickDie(values);
        int modifier = owner.pickInRange(-1, 1); //TODO allow a modifier of zero?

        Die die = this.dice.getUnusedDie(owner, target);
        this.modified = new DieModifier(die, modifier);
        this.dice.removeDie(owner, die);
        this.dice.addDie(owner, this.modified);

    }

    @Override
    public void endOfTurn() {
        this.dice.unwrap(this.modified);
    }
}
