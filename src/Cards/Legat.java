package Cards;

import Player.Player;
import RomaEngine.Bank;
import RomaEngine.DiceDiscs;
import RomaEngine.OpponentMap;

/**
 * User: timothyc
 * Date: 5/16/12
 * Time: 11:13 AM
 */
public class Legat extends AbstractCard {

    private Bank bank;
    private OpponentMap opponents;
    private DiceDiscs diceDiscs;
    
    public Legat(Bank bank, OpponentMap opponents, DiceDiscs diceDiscs) {
        super(CardID.LEGAT);
        this.bank = bank;
        this.opponents = opponents;
        this.diceDiscs = diceDiscs;
    }

    @Override
    public void activate(Player owner) {
        Player opponent = this.opponents.getOpponentOf(owner);
        int amount = this.diceDiscs.getUnoccupiedDiscs(opponent);
        this.bank.receiveStockpileVictoryPoints(owner, amount);
    }


}
