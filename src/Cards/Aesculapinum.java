package Cards;

import Player.Player;
import RomaEngine.Deck;

import java.util.List;
import java.util.Map;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 9:03 PM
 */
public class Aesculapinum extends AbstractCard {

    private Deck deck;
    private Map<Player, List<Card>> hands;

    public Aesculapinum(Deck deck, Map<Player, List<Card>> hands) {
        super(CardID.AESCULAPINUM);
        this.deck = deck;
        this.hands = hands;
    }

    @Override
    public void activate(Player owner) {
        List<Card> discards = this.deck.getDiscards();
        if (discards.isEmpty()) {
            owner.receiveNotification("cannot activate "+this.toString()+" -- the discard pile is empty");
        } else {
            Card selection = owner.pickCard(discards);
            assert(discards.remove(selection));
            //this.deck.setDiscards(discards); //TODO unnecessary? we got the original list, so it should be fine?
            this.hands.get(owner).add(selection);
        }
    }
}
