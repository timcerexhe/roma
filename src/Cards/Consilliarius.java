package Cards;

import Player.Player;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;

import java.util.LinkedList;
import java.util.List;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 1:02 AM
 */
public class Consilliarius extends AbstractCard {

    private DiceDiscs discs;

    public Consilliarius(DiceDiscs discs) {
        super(CardID.CONSILLIARIUS);
        this.discs = discs;
    }

    @Override
    public void activate(Player owner) {
        List<Card> cards = pickUpCharacterCards(owner);
        for (Card c : cards) {
            int i = owner.pickDiceDisc();
            DiceDisc d = this.discs.getDisc(i);
            d.layCard(owner, c);
        }
    }

    private List<Card> pickUpCharacterCards(Player owner) {
        List<Card> cards = new LinkedList<Card>();
        for (DiceDisc d : this.discs.getDiscs()) {
            Card c = d.getLaidCard(owner);
            if (c != null && c.getCardType() == CardType.CHARACTER) {
                assert(d.removeCardFromDiceDisc(owner) == c);
                cards.add(c);
            }
        }
        return cards;
    }
}
