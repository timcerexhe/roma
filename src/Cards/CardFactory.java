package Cards;

import RomaEngine.*;

import java.util.*;

/**
 * User: timothyc
 * Date: 5/15/12
 * Time: 6:52 PM
 */
public class CardFactory {

    private Engine engine;

    public CardFactory(Engine engine) {
        this.engine = engine;
    }

    public List<Card> newDeck() {
        List<Card> deck = new LinkedList<Card>();

        for (CardID id : CardID.values()) {
            deck.add(newCard(id)); //TODO some cards occur more than once
        }

        return deck;
    }

    public Card newCard(CardID id) {
        switch (id) {
            //character cards
            case SICARIUS:          return newSicarius();
            case ARCHITECTUS:       return newArchitectus();
            case CONSILLIARIUS:     return newConsilliarius();
            case LEGAT:             return newLegat();
            case GLADIATOR:         return newGladiator();
            case MERCATOR:          return newMercator();
            case CONSUL:            return newConsul();
            case LEGIONARIUS:       return newLegionarius();
            case NERO:              return newNero();
            case PRAETORIANUS:      return newPraetorianus();
            case SCAENICUS:         return newScaenicus();
            case HARUSPEX:          return newHaruspex();
            case SENATOR:           return newSenator();
            case VELITES:           return newVelites();
            case ESSEDUM:           return newEssedum();
            case TRIBUNUS_PLEBIS:   return newTribunusPlebis();
            case CENTURIO:          return newCenturio();

            //building cards
            case AESCULAPINUM:      return newAesculapinum();
            case BASILICA:          return newBasilica();
            case MACHINA:           return newMachina();
            case FORUM:             return newForum();
            case MERCATUS:          return newMercatus();
            case ONAGER:            return newOnager();
            case TEMPLUM:           return newTemplum();
            case TURRIS:            return newTurris();

            default:
                throw new RomaIsBurningException("Cannot create new card of unknown type '"+id+"'");
        }
    }

    public Sicarius newSicarius() {
        AddressOfArena arena = engine;
        return new Sicarius(engine.getDiceDiscs(), arena, engine.getOpponentMap());
    }

    public Architectus newArchitectus() {
        AddressOfEstateAgent agent = engine;
        return new Architectus(agent);
    }

    public Consilliarius newConsilliarius() {
        return new Consilliarius(engine.getDiceDiscs());
    }

    public Legat newLegat() {
        return new Legat(engine.getBank(), engine.getOpponentMap(), engine.getDiceDiscs());
    }

    public Gladiator newGladiator() {
        return new Gladiator(engine.getDiceDiscs(), engine.getOpponentMap(), engine.getHands());
    }

    public Mercator newMercator() {
        return new Mercator(engine.getBank(), engine.getOpponentMap());
    }

    public Consul newConsul() {
        return new Consul(engine.getDiceManager());
    }

    public Legionarius newLegionarius() {
        AddressOfArena arena = engine;
        return new Legionarius(engine.getDiceDiscs(), arena, engine.getOpponentMap());
    }

    public Nero newNero() {
        AddressOfArena arena = engine;
        return new Nero(engine.getDiceDiscs(), arena, engine.getOpponentMap());
    }

    public Praetorianus newPraetorianus() {
        return new Praetorianus(engine.getDiceDiscs(), engine.getOpponentMap());
    }

    public Scaenicus newScaenicus() {
        return new Scaenicus(engine.getDiceDiscs());
    }

    public Haruspex newHaruspex() {
        return new Haruspex(engine.getDeck(), engine.getHands());
    }

    public Senator newSenator() {
        AddressOfEstateAgent agent = engine;
        return new Senator(agent);
    }

    public Velites newVelites() {
        AddressOfArena arena = engine;
        return new Velites(engine.getDiceDiscs(), arena, engine.getOpponentMap());
    }

    public Essedum newEssedum() {
        AddressOfArena arena = engine;
        return new Essedum(arena, engine.getOpponentMap());
    }

    public TribunusPlebis newTribunusPlebis() {
        return new TribunusPlebis(engine.getBank());
    }

    public Centurio newCenturio() {
        return new Centurio();
    }

    public Aesculapinum newAesculapinum() {
        return new Aesculapinum(engine.getDeck(), engine.getHands());
    }

    public Basilica newBasilica() {
        return new Basilica();
    }

    public Machina newMachina() {
        return new Machina(engine.getDiceDiscs());
    }

    public Forum newForum() {
        return new Forum();
    }

    public Mercatus newMercatus() {
        return new Mercatus(engine.getBank(), engine.getDiceDiscs(), engine.getOpponentMap());
    }

    public Onager newOnager() {
        AddressOfArena arena = engine;
        return new Onager(engine.getDiceDiscs(), arena, engine.getOpponentMap());
    }

    public Templum newTemplum() {
        return new Templum();
    }

    public Turris newTurris() {
        AddressOfArena arena = engine;
        return new Turris(arena);
    }

}
