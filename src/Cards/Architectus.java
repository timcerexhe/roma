package Cards;

import Player.Player;
import RomaEngine.AddressOfEstateAgent;
import RomaEngine.EstateAgent;
import RomaEngine.FreeEstateAgent;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 6:40 PM
 */
public class Architectus extends AbstractCard {

    private AddressOfEstateAgent agent;
    private FreeEstateAgent wrapped;

    public Architectus(AddressOfEstateAgent agent) {
        super(CardID.ARCHITECTUS);
        this.agent = agent;
        this.wrapped = null;
    }

    @Override
    public void activate(Player owner) {
        this.wrapped = new FreeEstateAgent(agent.getAgent(), owner, false, true);
        this.agent.setAgent(this.wrapped);
    }

    @Override
    public void endOfTurn() {
        if (this.wrapped != null) {
            EstateAgent unwrapped = this.agent.getAgent().unwrap(this.wrapped);
            this.agent.setAgent(unwrapped);
        }
    }
}
