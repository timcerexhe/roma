package Cards;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 7:50 PM
 */
public enum CardType {
    CHARACTER, BUILDING;
}
