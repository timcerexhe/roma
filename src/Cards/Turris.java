package Cards;

import Player.Player;
import RomaEngine.AddressOfArena;
import RomaEngine.Arena;
import RomaEngine.HandicapArena;

import java.util.HashMap;
import java.util.Map;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 9:29 PM
 */
public class Turris extends AbstractCard {

    private AddressOfArena arena;
    private Arena wrapper;

    public Turris(AddressOfArena arena) {
        super(CardID.TURRIS);
        this.arena = arena;
        this.wrapper = null;
    }

    @Override
    public void onPlacement(Player owner, int discNumber) {
        Arena original = this.arena.getArena();
        Map<Player, Integer> handicap = new HashMap<Player, Integer>();
        handicap.put(owner, 1);
        this.wrapper = new HandicapArena(original, handicap);
        this.arena.setArena(this.wrapper);
    }

    @Override
    public void onRemove() {
        Arena a = this.arena.getArena();
        a = a.unwrap(this.wrapper);
        this.arena.setArena(a);
    }

    @Override
    public void activate(Player owner) {
        //does nothing
    }
}
