package Cards;

import Player.Player;
import RomaEngine.Deck;

import java.util.List;
import java.util.Map;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 7:21 PM
 */
public class Haruspex extends AbstractCard {

    private Deck deck;
    private Map<Player, List<Card>> hands;

    public Haruspex(Deck deck, Map<Player, List<Card>> hands) {
        super(CardID.HARUSPEX);
        this.deck = deck;
        this.hands = hands;
    }

    @Override
    public void activate(Player owner) {
        List<Card> theCards = this.deck.getDeck();
        Card selection = owner.pickCard(theCards);
        assert(theCards.remove(selection));
        this.deck.setDeck(theCards);
        this.hands.get(owner).add(selection);
    }
}
