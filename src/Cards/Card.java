package Cards;

import Player.Player;

/**
 * User: timothyc
 * Date: 5/15/12
 * Time: 6:51 PM
 */
public interface Card {

    public CardID getCardID();
    public CardType getCardType();
    public int getPrice();
    public int getDefenceLevel();

    public void onPlacement(Player owner, int discNumber);
    public void onRemove();
    public void activate(Player owner);
    public void endOfTurn();

}
