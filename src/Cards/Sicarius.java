package Cards;

import Player.Player;
import RomaEngine.AddressOfArena;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;
import RomaEngine.OpponentMap;

import java.util.List;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 11:57 PM
 */
public class Sicarius extends AbstractCard {

    private DiceDiscs discs;
    private AddressOfArena arena;
    private OpponentMap opponents;

    public Sicarius(DiceDiscs discs, AddressOfArena arena, OpponentMap opponents) {
        super(CardID.SICARIUS);
        this.discs = discs;
        this.arena = arena;
    }

    @Override
    public void activate(Player owner) {
        List<DiceDisc> discList = this.discs.getDiscs();
        Player opponent = this.opponents.getOpponentOf(owner);

        Card target = null;
        DiceDisc opponentDisc = null;

        while (target == null) {
            int discNumber = owner.pickDiceDisc();
            target = discList.get(discNumber).getLaidCard(opponent);
            opponentDisc = discList.get(discNumber);
        }

        DiceDisc ourDisc = getOurDisc(owner);
        assert(ourDisc != null);
        assert(opponentDisc != null);

        this.arena.getArena().discard(owner, ourDisc);
        this.arena.getArena().discard(opponent, opponentDisc);
    }

    private DiceDisc getOurDisc(Player owner) {
        for (DiceDisc disc : this.discs.getDiscs()) {
            if (disc.getLaidCard(owner) == this) {
                return disc;
            }
        }
        return null; //shouldn't happen --> indicates that this card either hasn't been laid, or isn't ours!
    }
}
