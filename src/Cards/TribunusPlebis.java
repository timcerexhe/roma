package Cards;

import Player.Player;
import RomaEngine.Bank;

/**
 * Created by IntelliJ IDEA.
 * User: timothyc
 * Date: 5/15/12
 * Time: 6:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class TribunusPlebis extends AbstractCard {

    private static final int TRIBUNUS_PLEBIS_VICTORY_POINTS = 1;

    private Bank bank;

    public TribunusPlebis(Bank bank) {
        super(CardID.TRIBUNUS_PLEBIS);
        this.bank = bank;
    }

    @Override
    public void activate(Player owner) {
        this.bank.transferVictoryPointsFromOpponent(owner, TRIBUNUS_PLEBIS_VICTORY_POINTS);
    }
}
