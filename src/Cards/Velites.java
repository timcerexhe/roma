package Cards;

import Player.Player;
import RomaEngine.AddressOfArena;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;
import RomaEngine.OpponentMap;

import java.util.List;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 7:40 PM
 */
public class Velites extends AbstractCard {

    private DiceDiscs discs;
    private AddressOfArena arena;
    private OpponentMap opponents;

    public Velites(DiceDiscs discs, AddressOfArena arena, OpponentMap opponents) {
        super(CardID.VELITES);
        this.discs = discs;
        this.arena = arena;
        this.opponents = opponents;
    }

    @Override
    public void activate(Player owner) {
        Player opponent = this.opponents.getOpponentOf(owner);
        int numCandidates = getNumCandidates(opponent, this.discs.getDiscs());

        if (numCandidates == 0) {
            owner.receiveNotification("cannot activate Velites -- "+opponent.getName()+" has no characters");

        } else {
            DiceDisc target = null;
            Card attacked = null;
            while (target == null) {
                int discNumber = owner.pickDiceDisc();
                DiceDisc selected = this.discs.getDisc(discNumber);
                attacked = selected.getLaidCard(opponent);
                if (attacked != null && attacked.getCardType() == CardType.CHARACTER) {
                    target = selected;
                }
            }

            DiceDisc thisDisc = this.discs.getDiscHolding(owner, this);
            this.arena.getArena().rollBattleDie();
            int attack = this.arena.getArena().getAttackLevel(owner, thisDisc);
            int defence = this.arena.getArena().getDefenceLevel(opponent, target);

            if (attack >= defence) {
                owner.receiveNotification("you have successfully attacked "+opponent.getName()+"'s "+attacked);
                opponent.receiveNotification("your "+attacked+" has been successfully attacked by "+owner.getName());
                this.arena.getArena().discard(opponent, target);
            } else {
                owner.receiveNotification("your attack on "+opponent.getName()+"'s "+attacked+" has failed");
                opponent.receiveNotification("your "+attacked+" has defended an attack from "+owner.getName());
            }
        }
    }

    private int getNumCandidates(Player opponent, List<DiceDisc> discs) {
        int count = 0;
        for (DiceDisc d : discs) {
            if (d.getLaidCard(opponent).getCardType() == CardType.CHARACTER) {
                count++;
            }
        }
        return count;
    }
}

