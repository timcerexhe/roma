package RomaEngine;

/**
 * Created by IntelliJ IDEA.
 * User: timothyc
 * Date: 5/15/12
 * Time: 7:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class GameOverException extends RuntimeException {
    public GameOverException(String msg) {
        super(msg);
    }
}
