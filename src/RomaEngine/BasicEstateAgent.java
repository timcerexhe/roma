package RomaEngine;

import Cards.Card;
import Player.Player;

import java.util.HashMap;
import java.util.List;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 1:28 AM
 */
public class BasicEstateAgent implements EstateAgent
{

    private Bank bank;
    private DiceDiscs discs;
    private HashMap<Player, List<Card>> hands;

    public BasicEstateAgent(Bank bank, DiceDiscs discs, HashMap<Player, List<Card>> hands) {
        this.bank = bank;
        this.discs = discs;
        this.hands = hands;
    }

    @Override
    public boolean layCard(Player p, Card c, int discNumber) {
        int cost = getCostToLay(p, c);
        int funds = this.bank.getPlayerSestertii(p);

        boolean success = false;
        if (cost <= funds) {
            if (discs.layCard(p, c, discNumber)) {
                this.bank.transferSestertiiToBank(p, cost);
                success = true;
            }
        } else {
            p.receiveNotification("insufficient funds to lay card");
        }
        return success;
    }

    @Override
    public int getCostToLay(Player p, Card c) {
        return c.getPrice();
    }

    @Override
    public void handleLayCard(Player p) {
        List<Card> hand = this.hands.get(p);
        Card lay = p.pickCard(hand);
        int discNumber = p.pickDiceDisc();
        if (layCard(p, lay, discNumber)) {
            assert(hand.remove(lay));
        }

    }

    @Override
    public EstateAgent unwrap(EstateAgent wrapper) {
        return this; //we are the base; nothing to unwrap
    }

}
