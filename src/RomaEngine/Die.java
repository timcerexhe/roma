package RomaEngine;

/**
 * User: timothyc
 * Date: 5/16/12
 * Time: 9:37 AM
 */
public interface Die {
    
    public int roll();

    public int getLastRoll();

    public Die unwrap(Die wrapper);

}
