package RomaEngine;

import Cards.Card;
import Player.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: timothyc
 * Date: 5/18/12
 * Time: 1:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class BasicArena implements Arena {

    private Die battleDie;
    private Deck deck;

    public BasicArena(Deck deck) {
        this.battleDie = new PlainDie();
        this.deck = deck;
    }

    @Override
    public int rollBattleDie() {
        return this.battleDie.roll();
    }

    @Override
    public void discard(Player owner, DiceDisc disc) {
        Card c = disc.removeCardFromDiceDisc(owner);
        this.deck.discard(c);
    }

    @Override
    public int getAttackLevel(Player attacker, DiceDisc attackingDisc) {
        return this.battleDie.getLastRoll();
    }

    @Override
    public int getDefenceLevel(Player defender, DiceDisc defendingDisc) {
        Card c = defendingDisc.getLaidCard(defender);
        assert(c != null);
        return c.getDefenceLevel();
    }

    @Override
    public Arena unwrap(Arena wrapper) {
        return this;
    }


}
