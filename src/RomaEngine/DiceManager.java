package RomaEngine;

import Player.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 3:26 PM
 */
public class DiceManager {

    private Map<Player, List<Die>> dice;
    private Map<Player, List<Die>> usedDice;

    public DiceManager(List<Player> players) {
        this.dice = new HashMap<Player, List<Die>>();
        this.usedDice = new HashMap<Player, List<Die>>();

        for (Player p : players) {
            List<Die> d = new ArrayList<Die>();
            for (int i = 0; i < Engine.NUM_PLAYER_DICE; i++) {
                d.add(new PlainDie());
            }
            this.dice.put(p, d);
            this.usedDice.put(p, new ArrayList<Die>());
        }
    }

    public void rollPlayersDice(Player p) {
        //first copy all the used ones back into the main pool
        for (Die d : this.usedDice.get(p)) {
            this.dice.get(p).add(d);
        }

        //forget that we ever used them
        this.usedDice.get(p).clear();

        //now roll all the dice
        for (Die d : this.dice.get(p)) {
            d.roll();
        }
    }

    public List<Integer> getRemainingDiceValues(Player p) {
        List<Integer> values = new ArrayList<Integer>();
        for (Die d : this.dice.get(p)) {
            values.add(d.getLastRoll());
        }
        return values;
    }

    public void useDieValue(Player p, int value) {
        Die d = getUnusedDie(p, value);
        assert(d != null);
        assert(this.dice.get(p).remove(d)); //pull it out of the candidates pool
        this.usedDice.get(p).add(d); //and add it to the used dice pool
    }

    public Die getUnusedDie(Player p, int target) {
        for (Die d : this.dice.get(p)) {
            if (d.getLastRoll() == target) {
                return d;
            }
        }
        return null;
    }


    public void removeDie(Player p, Die die) {
        assert(this.dice.get(p).remove(die));
    }

    public void addDie(Player p, Die die) {
        this.dice.get(p).add(die);
    }

    public void unwrap(Die wrapper) {
        //we can't overwrite a value in a List, so construct a new list instead
        //there are only a few dice and our decorator semantics are reference-based
        //so it is safe to loop through all (the player's) dice and try to unwrap
        //each in turn
        for (Player p : this.dice.keySet()) {
            List<Die> cleanDice = new ArrayList<Die>();
            for (Die d : this.dice.get(p)) {
                cleanDice.add(d.unwrap(wrapper));
            }
            this.dice.put(p, cleanDice);
        }

        //repeat for the unused dice, since the wrapped die may be there by now!
        for (Player p : this.usedDice.keySet()) {
            List<Die> cleanDice = new ArrayList<Die>();
            for (Die d : this.usedDice.get(p)) {
                cleanDice.add(d.unwrap(wrapper));
            }
            this.usedDice.put(p, cleanDice);
        }
    }
}
