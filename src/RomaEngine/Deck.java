package RomaEngine;

import Cards.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 7:26 PM
 */
public class Deck {

    private List<Card> deck;
    private List<Card> discards;

    public Deck() {
        this.deck = new LinkedList<Card>();
        this.discards = new LinkedList<Card>();
    }

    public Card deal() {
        Card c = this.deck.remove(0);
        if (this.deck.isEmpty()) {
            this.deck.addAll(this.discards);
            this.discards.clear();
        }
        return c;
    }

    public void setDeck(List<Card> cards) {
        this.deck.clear();
        this.deck.addAll(cards);
    }

    public List<Card> getDeck() {
        List<Card> cards = new ArrayList<Card>();
        cards.addAll(this.deck);
        return cards;
    }

    public void discard(Card c) {
        this.discards.add(c);
    }

    public void shuffle() {
        Collections.shuffle(this.deck);
    }

    public List<Card> getDiscards() {
        return this.discards;
    }

    public void setDiscards(List<Card> newDiscards) {
        this.discards.clear();
        this.discards.addAll(newDiscards);
    }
}
