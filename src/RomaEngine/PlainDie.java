package RomaEngine;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: timothyc
 * Date: 20/05/12
 * Time: 1:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class PlainDie implements Die {

    private static final int NO_PREVIOUS_ROLL = -1;

    private int lastRoll = NO_PREVIOUS_ROLL;
    private Random random;

    public PlainDie() {
        this.random = new Random();
    }

    public int roll() {
        this.lastRoll = this.random.nextInt(Engine.MAX_DICE_ROLL) + 1; //make it 1-indexed
        return this.lastRoll;
    }

    public int getLastRoll() {
        assert(this.lastRoll != NO_PREVIOUS_ROLL);
        return this.lastRoll;
    }

    @Override
    public Die unwrap(Die wrapper) {
        return this; //nothing left to unwrap
    }
}
