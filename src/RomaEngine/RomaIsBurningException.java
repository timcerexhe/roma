package RomaEngine;

/**
 * Used to indicate a fundamental configuration error with Roma
 *
 * User: timothyc
 * Date: 19/05/12
 * Time: 5:29 PM
 */
public class RomaIsBurningException extends RuntimeException {
    public RomaIsBurningException(String msg) {
        super(msg);
    }
}
