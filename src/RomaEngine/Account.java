package RomaEngine;

import Player.Player;

/**
 * Created by IntelliJ IDEA.
 * User: timothyc
 * Date: 5/15/12
 * Time: 7:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class Account {

    private Player owner;
    private int victoryPoints;
    private int sestertii;

    public Account(Player p, int startingVictoryPoints, int startingSestertii) {
        this.owner = p;
        this.victoryPoints = startingVictoryPoints;
        this.sestertii = startingSestertii;
    }

    public void transferVictoryPoints(int amount) {
        this.victoryPoints += amount;
        if (this.victoryPoints <= 0) {
            this.victoryPoints = 0;
            throw new GameOverException("player "+this.owner.getName()+" has run out of victory points");
        }
    }

    public void transferSestertii(int amount) {
        //don't go below zero!
        this.sestertii = Math.max(this.sestertii + amount, 0);
    }


    public int getVictoryPoints() {
        return this.victoryPoints;
    }

    public int getSestertii() {
        return this.sestertii;
    }
}
