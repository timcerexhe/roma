package RomaEngine;

import Cards.Card;
import Player.Player;

/**
 * Created with IntelliJ IDEA.
 * User: timothyc
 * Date: 19/05/12
 * Time: 1:27 AM
 * To change this template use File | Settings | File Templates.
 */
public interface EstateAgent {

    public boolean layCard(Player p, Card c, int discNumber);

    public int getCostToLay(Player p, Card c);

    public void handleLayCard(Player p);

    public EstateAgent unwrap(EstateAgent wrapper);

}
