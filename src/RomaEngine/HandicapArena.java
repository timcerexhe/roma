package RomaEngine;

import Cards.Card;
import Player.Player;

import java.util.List;
import java.util.Map;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 2:01 AM
 */
public class HandicapArena implements Arena {

    private Arena arena;
    private Map<Player, Integer> handicap;

    public HandicapArena(Arena base, Map<Player, Integer> handicap) {
        this.arena = base;
        this.handicap = handicap;
    }

    @Override
    public int rollBattleDie() {
        return this.arena.rollBattleDie();
    }

    @Override
    public void discard(Player owner, DiceDisc disc) {
        this.arena.discard(owner, disc);
    }

    @Override
    public int getAttackLevel(Player attacker, DiceDisc attackingDisc) {
        return this.arena.getAttackLevel(attacker, attackingDisc);
    }

    @Override
    public int getDefenceLevel(Player defender, DiceDisc defendingDisc) {
        Integer modifier = this.handicap.get(defender);
        if (modifier == null) {
            modifier = 0;
        }
        return this.getDefenceLevel(defender, defendingDisc) + modifier;
    }

    @Override
    public Arena unwrap(Arena wrapper) {
        if (this == wrapper) {
            return this.arena;
        } else {
            this.arena = this.arena.unwrap(wrapper);
            return this;
        }
    }

}
