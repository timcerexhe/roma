package RomaEngine;

import Cards.Card;
import Player.Player;

import java.util.List;

/**
 * User: timothyc
 * Date: 5/18/12
 * Time: 1:37 PM
 */
public interface Arena {

    public int rollBattleDie();

    public void discard(Player owner, DiceDisc disc);

    public int getAttackLevel(Player attacker, DiceDisc attackingDisc);

    public int getDefenceLevel(Player defender, DiceDisc defendingDisc);

    public Arena unwrap(Arena wrapper);

}
