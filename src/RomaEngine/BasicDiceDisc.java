package RomaEngine;

import Cards.Card;
import Player.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 1:56 AM
 */
public class BasicDiceDisc implements DiceDisc {

    private int number;
    private Map<Player, Card> cards;

    public BasicDiceDisc(int number) {
        this.number = number;
        this.cards = new HashMap<Player, Card>();
    }

    @Override
    public Card getLaidCard(Player p) {
        return cards.get(p);
    }

    @Override
    public Card removeCardFromDiceDisc(Player owner) {
        Card c = this.cards.remove(owner);
        assert(c != null);
        return c;
    }

    @Override
    public boolean layCard(Player p, Card c) {
        this.cards.put(p, c);
        return true;
    }

    @Override
    public DiceDisc unwrap(DiceDisc wrapper) {
        return this; //we are root
    }

}
