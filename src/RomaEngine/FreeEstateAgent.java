package RomaEngine;

import Cards.Card;
import Cards.CardType;
import Player.Player;
import RomaEngine.EstateAgent;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 7:46 PM
 */
public class FreeEstateAgent implements EstateAgent {

    private EstateAgent agent;
    private Player friendOfTheEstablishment;
    private boolean freeCharacterCards;
    private boolean freeBuildingCards;


    public FreeEstateAgent(EstateAgent agent, Player p, boolean freeCharacterCards, boolean freeBuildingCards) {
        this.agent = agent;
        this.friendOfTheEstablishment = p;
        this.freeCharacterCards = freeCharacterCards;
        this.freeBuildingCards = freeBuildingCards;
    }

    @Override
    public boolean layCard(Player p, Card c, int discNumber) {
        return this.agent.layCard(p, c, discNumber);
    }

    @Override
    public int getCostToLay(Player p, Card c) {
        if (isFree(p, c)) {
            return 0; //special price for you my friend: free! :)
        } else {
            return this.agent.getCostToLay(p, c);
        }
    }

    private boolean isFree(Player p, Card c) {
        boolean free = false;
        if (p == this.friendOfTheEstablishment) {
            CardType type = c.getCardType();
            if (type == CardType.CHARACTER && freeCharacterCards) {
                free = true;
            } else if (type == CardType.BUILDING && freeBuildingCards) {
                free = true;
            }
        }
        return free;
    }

    @Override
    public void handleLayCard(Player p) {
        this.agent.handleLayCard(p);
    }

    @Override
    public EstateAgent unwrap(EstateAgent wrapper) {
        if (wrapper == this) {
            return this.agent;
        } else {
            this.agent = this.agent.unwrap(wrapper);
            return this;
        }
    }
}
