package RomaEngine;

import Player.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: timothyc
 * Date: 5/15/12
 * Time: 6:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class Bank {

    private static final int TOTAL_VICTORY_POINTS = 36;
    private static final int STARTING_VICTORY_POINTS = 10;
    private static final int STARTING_SESTERTII = 0;
    private static final int BANK_STARTING_SESTERTII = Integer.MAX_VALUE;

    private HashMap<Player, Account> accounts;
    private OpponentMap opponents;
    private Account banksAccount;

    /**
     * Create a bank for controlling (and auditing!) player fees + funds
     *
     *  * players: the list of all players
     *  * opponents: the mapping from each player to all of their opponents (which may not be symmetric!)
     */
    public Bank(List<Player> players, OpponentMap opponents) {
        this.accounts = new HashMap<Player, Account>();
        this.opponents = opponents;
        int banksVictoryPoints = TOTAL_VICTORY_POINTS;

        for (Player p : players) {
            this.accounts.put(p, new Account(p, STARTING_VICTORY_POINTS, STARTING_SESTERTII));
            banksVictoryPoints -= STARTING_VICTORY_POINTS; //the bank gives (ie loses) these to the player
        }

        this.banksAccount = new Account(null, banksVictoryPoints, BANK_STARTING_SESTERTII);
    }

    public void transferVictoryPointsFromOpponent(Player p, int amount) {
        assert(amount > 0);
        Player opponent = this.opponents.getOpponentOf(p);
        transferVictoryPoints(opponent, p, amount);
    }

    public void transferVictoryPoints(Player from, Player to, int amount) {
        transferVictoryPoints(this.accounts.get(from), this.accounts.get(to), amount);
    }
    
    public void addVictoryPoints(Player owner, int amount) {
        this.accounts.get(owner).transferVictoryPoints(amount);
    }

    public void receiveStockpileVictoryPoints(Player p, int amount) {
        transferVictoryPoints(this.banksAccount, this.accounts.get(p), amount);
    }

    public void transferVictoryPointsToStockpile(Player p, int count) {
        assert(count > 0);
        transferVictoryPoints(this.accounts.get(p), this.banksAccount, count);
    }

    private void transferVictoryPoints(Account from, Account to, int amount) {
        //transfer the positive balance first so a player gets full credit even
        //if it bankrupts their opponent (as per rules)
        if (amount >= 0) {
            to.transferVictoryPoints(amount);
            from.transferVictoryPoints(-amount);
        } else {
            from.transferVictoryPoints(-amount);
            to.transferVictoryPoints(amount);
        }
    }


    public Player getPlayerWithMostVictoryPoints() {
        Player withTheMostest = null;
        int checkMaBling = -1;

        System.out.println("victory points:");
        for (Account a : this.accounts.values()) {
            System.out.println(" * " + a.getVictoryPoints());
        }

        for (Map.Entry<Player, Account> item : this.accounts.entrySet()) {
            int vp = item.getValue().getVictoryPoints();
            if (vp > checkMaBling || withTheMostest == null) {
                withTheMostest = item.getKey();
                checkMaBling = vp;
            }
        }
        return withTheMostest;
    }

    public int getPlayerSestertii(Player p) {
        return this.accounts.get(p).getSestertii();
    }

    public int getPlayerVictoryPoints(Player p) {
        return this.accounts.get(p).getVictoryPoints();
    }

    public void transferSestertiiToBank(Player p, int cost) {
        assert(cost >= 0);
        Account account = this.accounts.get(p);

        //pull the money out first
        account.transferSestertii(-cost);

        //give it to the bank, though technically we don't really care!
        this.banksAccount.transferSestertii(cost);
    }


    public void transferSestertiiFromBank(Player p, int amount) {
        assert(amount >= 0);
        Account account = this.accounts.get(p);

        //pull the money out first, though technically we don't really care about the bank's credit!
        this.banksAccount.transferSestertii(-amount);

        //give it to the player
        account.transferSestertii(amount);
    }

    public void transferSestertii(Player from, Player to, int amount) {
        transferSestertii(this.accounts.get(from), this.accounts.get(to), amount);
    }

    private void transferSestertii(Account from, Account to, int amount) {
        //transfer the positive balance first so a player gets full credit even
        //if it bankrupts their opponent (as per rules)
        if (amount >= 0) {
            to.transferSestertii(amount);
            from.transferSestertii(-amount);
        } else {
            from.transferSestertii(-amount);
            to.transferSestertii(amount);
        }
    }
}
