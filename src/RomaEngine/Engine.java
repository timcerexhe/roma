package RomaEngine;

import Cards.Card;
import Cards.CardFactory;
import Player.PlayerMoves;
import Player.Player;
import Player.AIPlayer;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: timothyc
 * Date: 5/15/12
 * Time: 6:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class Engine implements AddressOfEstateAgent, AddressOfArena {

    public static final int NUM_PLAYERS = 2;
    public static final int NUM_PLAYER_DICE = 3;
    public static final int NUM_DICE = NUM_PLAYERS * NUM_PLAYER_DICE;
    public static final int MAX_DICE_ROLL = 6;
    public static final int NUM_DICE_DISCS = MAX_DICE_ROLL;
    
    public static final int INITIAL_DEAL_SIZE = 4;
    public static final int NUM_CARDS_TO_SWAP = 2;

    private List<Player> players;
    private OpponentMap opponents;
    private Bank bank;
    private Arena arena;
    private Deck deck;
    private HashMap<Player, List<Card>> hands;
    private DiceDiscs diceDiscs;
    private DiceManager dice;
    private EstateAgent agent;
    private CardFactory factory;

    public Engine(List<Player> players) throws RomaIsBurningException {
        this.players = players;

        //sort the players list, so now it defines the order of play!
        Collections.sort(this.players);

        //work out who we're playing against
        this.opponents = new OpponentMap(this.players);

        //set up us the base
        this.bank = new Bank(this.players, this.opponents);
        this.hands = new HashMap<Player, List<Card>>();
        this.diceDiscs = new DiceDiscs();
        this.agent = new BasicEstateAgent(this.bank, this.diceDiscs, this.hands);
        this.dice = new DiceManager(this.players);

        this.factory = new CardFactory(this);
        this.deck = new Deck();
        this.deck.setDeck(this.factory.newDeck());

        this.arena = new BasicArena(this.deck);
    }

    //lame getters for the CardFactory
    public Bank getBank() {
        return this.bank;
    }

    public void setArena(Arena newArena) {
        this.arena = newArena;
    }

    public Arena getArena() {
        return this.arena;
    }

    public OpponentMap getOpponentMap() {
        return this.opponents;
    }

    public DiceDiscs getDiceDiscs() {
        return this.diceDiscs;
    }

    public void setAgent(EstateAgent newAgent) {
        this.agent = newAgent;
    }

    public EstateAgent getAgent() {
        return this.agent;
    }

    public DiceManager getDiceManager() {
        return this.dice;
    }

    public HashMap<Player,List<Card>> getHands() {
        return this.hands;
    }

    public Deck getDeck() {
        return this.deck;
    }

    //do the pre-game dance: shuffle deck, swap cards, sacrifice noble virgins, ...
    private void setupGame() {
        //shuffle deck
        this.deck.shuffle();

        //deal cards to each player
        for (Player p : this.players) {
            List<Card> hand = new ArrayList<Card>();
            for (int i = 0; i < INITIAL_DEAL_SIZE; i++) {
                hand.add(this.deck.deal());
            }
            this.hands.put(p, hand);
        }
        
        //choose cards to swap
        //each player chooses some cards from their hand ...
        HashMap<Player, List<Card>> swap = new HashMap<Player, List<Card>>();
        for (Player p : this.players) {
            List<Card> swapList = new ArrayList<Card>();
            for (int i = 0; i < NUM_CARDS_TO_SWAP; i++) {
                Card wantToSwap = p.pickDiscard(this.hands.get(p));
                assert(this.hands.get(p).contains(wantToSwap));
                this.hands.get(p).remove(wantToSwap);
                swapList.add(wantToSwap);
            }

            swap.put(p, swapList);
        }

        // ...and gives them to the opponent
        for (Player p : this.players) {
            this.hands.get(p).addAll(swap.get(opponents.getOpponentOf(p)));
        }
        
        //finally, players lay their cards against dice discs
        for (Player p : this.players) {
            List<Card> hand = this.hands.get(p);
            for (Card c : hand) {
                int discNumber = p.pickDiceDisc();
                this.diceDiscs.layCard(p, c, discNumber);
            }
        }
    }

    //take off every zig!
    public Player play() {
        setupGame();

        boolean gameOver = false;
        while (!gameOver) {
            gameOver = playOneRound();
        }

        return this.bank.getPlayerWithMostVictoryPoints();
    }

    private boolean playOneRound() {
        boolean gameOver = false;
        try {

            for (Player p : this.players) {
                System.out.println("hey "+p.getName()+"! play one round!");
                //phase 1
                penalisedUnoccupiedDiceDiscs(p);

                //phase 2
                this.dice.rollPlayersDice(p);

                //phase 3
                carryOutActions(p);
            }

        } catch (GameOverException e) {
            gameOver = true;
        }
        return gameOver;
    }

    private void carryOutActions(Player p) { //TODO gah! who created this!? (we need it, but when did it happen!? :s)
        boolean endOfTurn = false;

        while (!endOfTurn) {
            PlayerMoves move = p.nextMove();
            boolean hasMoreDice = !this.dice.getRemainingDiceValues(p).isEmpty();

            if (move == PlayerMoves.ACTIVATE_CARD && hasMoreDice) {
                int discNumber = p.pickDie(this.dice.getRemainingDiceValues(p));

                DiceDisc disc = this.diceDiscs.getDisc(discNumber);
                Card card = disc.getLaidCard(p);

                if (card != null) {
                    this.dice.useDieValue(p, discNumber); //only spend the die if it's legal
                    card.activate(p);
                } else {
                    p.receiveNotification("cannot activate empty dice disc " + discNumber);
                }

            } else if (move == PlayerMoves.LAY_CARD) {
                this.agent.handleLayCard(p);

            } else if (move == PlayerMoves.TAKE_MONEY && hasMoreDice) {
                int amount = p.pickDie(this.dice.getRemainingDiceValues(p));
                this.dice.useDieValue(p, amount);
                this.bank.transferSestertiiFromBank(p, amount);

            } else if (move == PlayerMoves.TAKE_CARD && hasMoreDice) {
                int amount = p.pickDie(this.dice.getRemainingDiceValues(p));
                this.dice.useDieValue(p, amount);
                List<Card> selection = new ArrayList<Card>();
                for (int i = 0; i < amount; i++) {
                    Card c = this.deck.deal();
                    assert(c != null);
                    selection.add(c);
                }

                Card keep = p.pickCard(selection);
                this.hands.get(p).add(keep);

            } else {
                assert(move == PlayerMoves.END_TURN || !hasMoreDice);
                endOfTurn = true; //put us out of our misery
            }
        }
    }

    private void penalisedUnoccupiedDiceDiscs(Player p) {
        int count = this.diceDiscs.getUnoccupiedDiscs(p);
        this.bank.transferVictoryPointsToStockpile(p, count);
    }

    public static void main(String[] args) throws RomaIsBurningException {
        Player[] players = { new AIPlayer("Mr. Roboto", 12), new AIPlayer("Destructo!", 3) };
        Engine e = new Engine(Arrays.asList(players));
        Player winner = e.play();
        System.out.println("winner is "+winner.getName()+", congratulations!");
    }

}
