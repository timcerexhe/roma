package RomaEngine;

/**
 * User: timothyc
 * Date: 20/05/12
 * Time: 1:57 AM
 */
public class DieModifier implements Die {

    private Die base;
    private int modifier;

    public DieModifier(Die original, int modifier) {
        this.base = original;
        this.modifier = modifier;
    }

    @Override
    public int roll() {
        return this.base.roll();
    }

    @Override
    public int getLastRoll() {
        return this.base.getLastRoll() + modifier;
    }

    @Override
    public Die unwrap(Die wrapper) {
        if (wrapper == this) {
            return this.base;
        } else {
            this.base = this.base.unwrap(wrapper);
            return this;
        }
    }
}
