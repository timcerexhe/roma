package RomaEngine;

import Cards.Card;
import Player.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * User: timothyc
 * Date: 5/16/12
 * Time: 9:36 AM
 */
public interface DiceDisc {

    public Card getLaidCard(Player p);

    public Card removeCardFromDiceDisc(Player owner);

    public boolean layCard(Player p, Card c);

    public DiceDisc unwrap(DiceDisc wrapper);
}
