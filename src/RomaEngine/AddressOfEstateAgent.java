package RomaEngine;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 11:03 PM
 */
public interface AddressOfEstateAgent {

    public EstateAgent getAgent();

    public void setAgent(EstateAgent newAgent);

}
