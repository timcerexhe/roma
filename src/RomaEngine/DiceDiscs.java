package RomaEngine;

import Cards.Card;
import Cards.Legionarius;
import Player.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: timothyc
 * Date: 5/16/12
 * Time: 12:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class DiceDiscs {

    private List<DiceDisc> discs;
    
    public DiceDiscs() {
        //NB. dice discs are numbered from ONE!
        this.discs = new ArrayList<DiceDisc>();
        for (int i = 1; i <= Engine.NUM_DICE_DISCS; i++) {
            this.discs.add(new BasicDiceDisc(i));
        }
    }

    public int getOccupiedDiscs(Player p) {
        return this.discs.size() - getUnoccupiedDiscs(p);
    }

    public int getUnoccupiedDiscs(Player p) {
        int count = 0;
        for (DiceDisc disc : this.discs) {
            if (disc.getLaidCard(p) == null) {
                count++;
            }
        }
        return count;
    }

    public boolean layCard(Player p, Card c, int discNumber) {
        DiceDisc d = getDisc(discNumber);
        assert(d != null);
        return d.layCard(p, c);
    }

    public List<DiceDisc> getDiscs() {
        return this.discs;
    }

    public DiceDisc getDisc(int discNumber) {
        return this.discs.get(discNumber-1);
    }


    public DiceDisc getDiscHolding(Player player, Card card) {
        for (DiceDisc d : this.discs) {
            if (d.getLaidCard(player) == card) {
                return d;
            }
        }
        return null;
    }

}
