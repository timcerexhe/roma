package RomaEngine;

import Player.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by IntelliJ IDEA.
 * User: timothyc
 * Date: 5/16/12
 * Time: 10:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class OpponentMap {
    
    private Map<Player, Player> opponents;
    
    public OpponentMap(List<Player> players) throws RomaIsBurningException {
        if (players.size() != Engine.NUM_PLAYERS) {
            throw new RomaIsBurningException("there must be two players");
        }

        this.opponents = new HashMap<Player, Player>();
        this.opponents.put(players.get(0), players.get(1));
        this.opponents.put(players.get(1), players.get(0));
    }
    
    public Player getOpponentOf(Player p) {
        return this.opponents.get(p);
    }
}
