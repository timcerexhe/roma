package RomaEngine;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 11:23 PM
 */
public interface AddressOfArena {

    public Arena getArena();

    public void setArena(Arena newArena);

}
