package Tests;

import Cards.Card;
import Player.Player;
import Player.PlayerMoves;
import Player.AbstractPlayer;
import RomaEngine.DiceDisc;
import RomaEngine.Die;

import java.util.List;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 6:04 PM
 */
public class MockPlayer extends AbstractPlayer {
    public MockPlayer(String name, int age) {
        super(name, age);
    }

    @Override
    public PlayerMoves nextMove() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Card pickCard(List<Card> cards) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Card pickDiscard(List<Card> cards) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int pickDiceDisc() {
        return 1;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int pickDie(List<Integer> playerDice) {
        return 1;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int pickInRange(int min, int max) {
        return min;
    }

}
