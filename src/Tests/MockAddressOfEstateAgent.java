package Tests;

import RomaEngine.AddressOfEstateAgent;
import RomaEngine.EstateAgent;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 11:42 PM
 */
public class MockAddressOfEstateAgent implements AddressOfEstateAgent {

    EstateAgent agent;

    public MockAddressOfEstateAgent(EstateAgent realAgent) {
        this.agent = realAgent;
    }

    @Override
    public EstateAgent getAgent() {
        return this.agent;
    }

    @Override
    public void setAgent(EstateAgent newAgent) {
        this.agent = newAgent;
    }
}
