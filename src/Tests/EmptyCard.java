package Tests;

import Cards.Card;
import Cards.CardID;
import Cards.CardType;
import Player.Player;

/**
 * Created with IntelliJ IDEA.
 * User: timothyc
 * Date: 19/05/12
 * Time: 5:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class EmptyCard implements Card {

    @Override
    public void activate(Player owner) {
        //does nothing
    }

    @Override
    public int getPrice() {
        return 0;
    }

    @Override
    public int getDefenceLevel() {
        return 0;
    }

    @Override
    public void onPlacement(Player owner, int discNumber) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onRemove() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public CardID getCardID() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public CardType getCardType() {
        return null;
    }

    @Override
    public void endOfTurn() {
    }
}
