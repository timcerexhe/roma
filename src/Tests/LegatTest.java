package Tests;

import Cards.Legat;
import RomaEngine.Bank;
import Player.Player;
import RomaEngine.DiceDiscs;
import RomaEngine.OpponentMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 6:00 PM
 */
public class LegatTest {

    private List<Player> players;
    private Bank bank;
    private Legat legat;

    @Before
    public void setUp() throws Exception {
        this.players = new ArrayList<Player>();
        players.add(new MockPlayer("a", 1));
        players.add(new MockPlayer("b", 2));
        OpponentMap opponents = new OpponentMap(players);
        this.bank = new Bank(players, opponents);
        DiceDiscs diceDiscs = new DiceDiscs();

        this.legat = new Legat(bank, opponents, diceDiscs);
    }

    @Test
    public void testActivate() throws Exception {
        Player a = this.players.get(0);
        Player b = this.players.get(1);

        int aMoney = this.bank.getPlayerVictoryPoints(a);
        int bMoney = this.bank.getPlayerVictoryPoints(b);
        assertEquals(10, aMoney);
        assertEquals(10, bMoney);

        this.legat.activate(a);

        aMoney = this.bank.getPlayerVictoryPoints(a);
        bMoney = this.bank.getPlayerVictoryPoints(b);
        assertEquals(16, aMoney);
        assertEquals(10, bMoney);

        this.legat.endOfTurn();

        aMoney = this.bank.getPlayerVictoryPoints(a);
        bMoney = this.bank.getPlayerVictoryPoints(b);
        assertEquals(16, aMoney);
        assertEquals(10, bMoney);
    }

    @Test
    public void testGetPrice() throws Exception {
        assertEquals(5, this.legat.getPrice());
    }

    @Test
    public void testGetDefenceLevel() throws Exception {
        assertEquals(2, this.legat.getDefenceLevel());
    }
}
