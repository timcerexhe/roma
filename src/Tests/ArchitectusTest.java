package Tests;

import Cards.Architectus;
import Cards.Card;
import Cards.Forum;
import Cards.Legat;
import Player.Player;
import RomaEngine.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 11:30 PM
 */
public class ArchitectusTest {

    private List<Player> players;
    private OpponentMap opponents;
    private Bank bank;
    private HashMap<Player, List<Card>> hands;
    private AddressOfEstateAgent agent;
    private DiceDiscs discs;
    private Architectus architectus;

    @Before
    public void setUp() throws Exception {

        this.players = new ArrayList<Player>();
        Player playerA = new MockPlayer("a", 1);
        Player playerB = new MockPlayer("b", 2);
        this.players.add(playerA);
        this.players.add(playerB);
        this.opponents = new OpponentMap(this.players);
        this.bank = new Bank(this.players, this.opponents);
        this.hands = new HashMap<Player, List<Card>>();
        this.hands.put(playerA, new ArrayList<Card>());
        this.hands.put(playerB, new ArrayList<Card>());

        EstateAgent realAgent = new BasicEstateAgent(this.bank, this.discs, this.hands);
        this.agent = new MockAddressOfEstateAgent(realAgent);

        this.discs = new DiceDiscs();

        this.architectus = new Architectus(this.agent);

        this.hands.get(playerA).add(this.architectus);

    }

    @Test
    public void testActivate() throws Exception {
        Player playerA = this.players.get(0);
        Player playerB = this.players.get(1);

        assertEquals(1, this.agent.getAgent().getCostToLay(playerA, this.architectus));
        this.architectus.activate(playerA);

        Legat legat = new Legat(this.bank, this.opponents, this.discs);
        Forum forum = new Forum();

        assertEquals(legat.getPrice(), this.agent.getAgent().getCostToLay(playerA, legat));
        assertEquals(legat.getPrice(), this.agent.getAgent().getCostToLay(playerB, legat));
        assertEquals(0, this.agent.getAgent().getCostToLay(playerA, forum));
        assertEquals(forum.getPrice(), this.agent.getAgent().getCostToLay(playerB, forum));

        this.architectus.endOfTurn();

        assertEquals(legat.getPrice(), this.agent.getAgent().getCostToLay(playerA, legat));
        assertEquals(legat.getPrice(), this.agent.getAgent().getCostToLay(playerB, legat));
        assertEquals(forum.getPrice(), this.agent.getAgent().getCostToLay(playerA, forum));
        assertEquals(forum.getPrice(), this.agent.getAgent().getCostToLay(playerB, forum));

    }

    @Test
    public void testGetPrice() throws Exception {
        assertEquals(1, this.architectus.getPrice());
    }

    @Test
    public void testGetDefenceLevel() throws Exception {
        assertEquals(2, this.architectus.getDefenceLevel());
    }
}
