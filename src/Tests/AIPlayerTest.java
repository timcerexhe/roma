package Tests;

import Cards.Card;
import Player.AIPlayer;
import Player.Player;
import RomaEngine.BasicDiceDisc;
import RomaEngine.DiceDisc;
import RomaEngine.Engine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 5:18 PM
 */
public class AIPlayerTest {

    private AIPlayer player;

    @Before
    public void setup() {
        this.player = new AIPlayer("ai player", 5);
    }

    @After
    public void tearDown() {}

    @Test
    public void testPickCard() throws Exception {
        List<Card> cards;

        cards = new ArrayList<Card>();
        Card expected = new EmptyCard();
        cards.add(expected);

        int size = cards.size();
        Card c = this.player.pickCard(cards);

        assertEquals(size, cards.size());
        assertTrue(cards.contains(c));
        assertEquals(expected, c);

        cards = new LinkedList<Card>(); //try a different list type!
        cards.add(new EmptyCard());
        cards.add(new EmptyCard());
        cards.add(new EmptyCard());

        size = cards.size();
        c = this.player.pickCard(cards);

        assertEquals(size, cards.size());
        assertTrue(cards.contains(c));
    }

    @Test
    public void testPickCardFromEmpty() throws Exception {
        List<Card> cards;

        cards = new ArrayList<Card>();
        try {
            this.player.pickCard(cards);
            fail("pickCards requires at least one card");
        } catch (IllegalArgumentException e) {}
    }

    @Test
    public void testPickDiscard() throws Exception {
        List<Card> cards = new ArrayList<Card>();
        Card expected = new EmptyCard();
        cards.add(expected);
        Card selection = this.player.pickDiscard(cards);
        assertEquals(expected, selection);

        cards = new LinkedList<Card>();
        cards.add(new EmptyCard());
        cards.add(new EmptyCard());
        cards.add(new EmptyCard());
        selection = this.player.pickDiscard(cards);
        assertNotNull(selection);
        assertTrue(cards.contains(selection));
    }

    @Test
    public void testPickEmptyDiscard() throws Exception {
        List<Card> cards = new ArrayList<Card>();
        try {
            this.player.pickDiscard(cards);
            fail("pickDiscard requires at least one card");
        } catch (IllegalArgumentException e) {}
    }

    @Test
    public void testPickDiceDisc() throws Exception {
        for (int i = 0; i < 100000; i++) {
            int selection = this.player.pickDiceDisc();
            assertTrue(1 <= selection && selection <= Engine.NUM_DICE_DISCS);
        }
    }

    @Test
    public void testGetName() throws Exception {
        assertNotNull(this.player.getName());
    }

    @Test
    public void testCompareTo() throws Exception {
        Player p1 = new AIPlayer("age 0", 0);
        Player p2 = new AIPlayer("age -5", -5);
        Player p3 = new AIPlayer("age 100", 100);
        List<Player> players = new ArrayList<Player>();
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(this.player);

        Collections.sort(players);

        Player[] expected = { p2, p1, this.player, p3 };
        assertArrayEquals(expected, players.toArray());
    }

}
