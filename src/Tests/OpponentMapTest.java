package Tests;

import Player.Player;
import Player.AIPlayer;
import RomaEngine.GameOverException;
import RomaEngine.OpponentMap;
import RomaEngine.RomaIsBurningException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 4:59 PM
 */
public class OpponentMapTest {

    @After
    public void tearDown() throws Exception {}

    @Before
    public void setUp() throws Exception {}

    @Test
    public void testGetOpponentOf() throws Exception {
        List<Player> players = new ArrayList<Player>();
        Player player1 = new AIPlayer("player 1", 0);
        Player player2 = new AIPlayer("player 2", 1);
        players.add(player1);
        players.add(player2);
        OpponentMap opponent = new OpponentMap(players);

        assertEquals(player2, opponent.getOpponentOf(player1));
        assertEquals(player1, opponent.getOpponentOf(player2));
        assertNotSame(player1, opponent.getOpponentOf(player1));
        assertNotSame(player2, opponent.getOpponentOf(player2));
    }

    @Test
    public void testCreateWithOnePlayer() throws Exception {
        List<Player> players = new ArrayList<Player>();
        players.add(new AIPlayer("player 1", 0));
        try {
            new OpponentMap(players);
            fail("opponent map should expect 2 players");
        } catch (RomaIsBurningException e) {}
    }

    @Test
    public void testCreateWithThreePlayers() throws Exception {
        List<Player> players = new ArrayList<Player>();
        players.add(new AIPlayer("player 1", 0));
        players.add(new AIPlayer("player 2", 1));
        players.add(new AIPlayer("player 3", 2));
        try {
            new OpponentMap(players);
            fail("opponent map should expect 2 players");
        } catch (RomaIsBurningException e) {}
    }

    @Test
    public void testCreatePlayersWithSameName() throws Exception {
        List<Player> players = new ArrayList<Player>();
        players.add(new AIPlayer("player", 10));
        players.add(new AIPlayer("player", 100));
        OpponentMap opponent = new OpponentMap(players);
        //success :)
    }

    @Test
    public void testCreatePlayersWithSameAge() throws Exception {
        List<Player> players = new ArrayList<Player>();
        players.add(new AIPlayer("player1", 0));
        players.add(new AIPlayer("player2", 0));
        OpponentMap opponent = new OpponentMap(players);
        //success :)
    }

    @Test //(expected = GameOverException.class)
    public void testCreateNegativeAge() throws Exception { //verdict: let it happen! :)
        List<Player> players = new ArrayList<Player>();
        players.add(new AIPlayer("player1", 0));
        players.add(new AIPlayer("player2", -10));
        OpponentMap opponent = new OpponentMap(players);
    }
}
