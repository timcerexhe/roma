package Player;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 6:48 PM
 */
public enum PlayerMoves {
    LAY_CARD, TAKE_MONEY, TAKE_CARD, ACTIVATE_CARD, END_TURN;
}
