package Player;

import Cards.Card;
import RomaEngine.DiceDisc;

import java.util.List;

/**
 * User: timothyc
 * Date: 19/05/12
 * Time: 1:47 AM
 */
public abstract class AbstractPlayer implements Player {

    private String name;
    private int age;

    public AbstractPlayer(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getAge() {
        return this.age;
    }

    @Override
    public void receiveNotification(String s) {
        //do nothing ... most players aren't actually real :)
    }

    @Override
    public int compareTo(Player player) {
        return (this.age - player.getAge());
    }

}

