package Player;

import Cards.Card;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;
import RomaEngine.Die;

import java.util.List;

/**
 * User: timothyc
 * Date: 5/15/12
 * Time: 6:50 PM
 */
public interface Player extends Comparable<Player> {

    public String getName();

    public int getAge();

    public PlayerMoves nextMove();

    public Card pickCard(List<Card> cards);

    public Card pickDiscard(List<Card> cards);

    public int pickDiceDisc();

    int pickDie(List<Integer> playerDice);

    void receiveNotification(String s);

    int pickInRange(int min, int max);

    @Override
    public int compareTo(Player player);

}
