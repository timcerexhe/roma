package Player;

import Cards.Card;
import RomaEngine.DiceDisc;
import RomaEngine.DiceDiscs;
import RomaEngine.Die;
import RomaEngine.Engine;

import java.util.List;
import java.util.Random;

/**
 * User: timothyc
 * Date: 5/15/12
 * Time: 7:33 PM
 */
public class AIPlayer extends AbstractPlayer {

    private Random random;

    public AIPlayer(String name, int age) {
        super(name, age);
        this.random = new Random();
    }

    @Override
    public PlayerMoves nextMove() {
        PlayerMoves[] moves = PlayerMoves.values();
        int index = this.random.nextInt(moves.length);
        return moves[index];
    }

    @Override
    public Card pickCard(List<Card> cards) {
        int index = this.random.nextInt(cards.size());
        return cards.get(index);
    }

    @Override
    public Card pickDiscard(List<Card> cards) {
        int choice = this.random.nextInt(cards.size());
        return cards.get(choice);
    }

    @Override
    public int pickDiceDisc() {
        return this.random.nextInt(Engine.NUM_DICE_DISCS) + 1; //remember 1-based indexing
    }

    @Override
    public int pickDie(List<Integer> playerDice) {
        int index = this.random.nextInt(playerDice.size());
        return playerDice.get(index);
    }

    @Override
    public int pickInRange(int min, int max) {
        int range = max - min + 1; //make it inclusive!
        return min + this.random.nextInt(range);
    }


}
